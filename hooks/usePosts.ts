import { useEffect, useState } from 'react';
import { getPosts } from '../pages/api/post';

import { IProps as IPost } from './../components/post/post';

const usePosts = () => {
  const [posts, setPosts] = useState<IPost[]>([]);
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    const load = async () => {
      const { data } = await getPosts();
      
      const posts: IPost[] = data?.feed?.items?.map((item: any) => ({
        id: item.id,
        title: item.title,
        description: item.description,
        date: new Date(item.date),
        createdAt: new Date(item.createdAt),
        image: item.enclosure.url
      }));

      setPosts(posts);
      console.log(posts);
      setLoading(false);
    };

    load();
  }, []);

  return {
    loading,
    posts
  };
};

export default usePosts;