import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import {Hidden, Typography, Card, 
      CardActionArea, CardContent, CardMedia} from '@material-ui/core'
import moment from "moment"

const useStyles = makeStyles(theme => ({
  card: {
    display: 'flex',
    margin: 10
  },
  cardDetails: {
    flex: 1
  },
  cardMedia: {
    height: 150
  }
}))

export interface IProps {
  id: string;
  title: string;
  description: string;
  date: Date;
  createdAt: Date;
  image: string;
}

const Post = ({ id, title, description, date, createdAt, image }: IProps) => {
  const classes = useStyles()

  const start = moment(createdAt);
  const duration = moment.duration(start.diff(date));
  const hoursLong = duration.asHours();
  const minutesLong = duration.asMinutes();
  const hoursToString = String(hoursLong);
  const minutesToString = String(minutesLong);
  const hoursShort = parseInt(hoursToString);
  const minutesShort = parseInt(minutesToString);
  const difference = Number(hoursLong) > 0 ? (`${minutesShort}m`)  : (`${hoursShort}h`) 

 return (
    <CardActionArea component="a" href="#" key={id}>
      <Card className={classes.card}>
        <div className={classes.cardDetails} >
        <Hidden xsDown>
          <CardMedia
            className={classes.cardMedia}
            image={image}
            title="Image title"
          />
        </Hidden> 
        <CardContent>
          <Typography component="h2" variant="h5">
            {title}  
          </Typography>
          <Typography variant="subtitle1" >
          {description}
          </Typography>
          <Typography variant="subtitle1" color="textSecondary">
            cnn •   {difference}
          </Typography>
        </CardContent>
        </div>
      </Card>
    </CardActionArea>
  )
} 

export default Post