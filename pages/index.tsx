import React from 'react'
import Masonry from 'react-masonry-css'
import { makeStyles } from '@material-ui/core/styles'
import {CssBaseline, Typography, Grid, CircularProgress} from '@material-ui/core'
import Post from "../components/post";
import usePosts from '../hooks/usePosts';

import { IProps as IPost } from '../components/post/post';

const useStyles = makeStyles(theme => ({
  toolbarTitle: {
    flex: 1,
    padding: 20
  }
}))

const Blog = () => {
  const classes = useStyles()
  const { loading, posts } = usePosts();
  const breakpoints = {
    default: 4,
    1100:2,
    700: 1
  }

  return (
    <>
      <CssBaseline />
      <Typography
        component="h2"
        variant="h5"
        color="inherit"
        align="center"
        noWrap
        className={classes.toolbarTitle}
      >
        CNN - Breaking News, Latest News and Videos
      </Typography>

      <main>
        <Grid container spacing={2} >
          {
            loading ? <CircularProgress /> 
            : (
              <Masonry 
                breakpointCols={breakpoints}
                className="my-masonry-grid"
                columnClassName="my-masonry-grid_column"
              >
                {posts.map((item: IPost, i) => (
                  <Post {...item} key={i} />
                ))}
              </Masonry>
            )
          }
        </Grid>
      </main>
    </>
  )
} 

export default Blog