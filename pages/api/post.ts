import { API_URL } from '../../constants/api';
import axios from 'axios';

export const getPosts = async () => axios.get(API_URL);